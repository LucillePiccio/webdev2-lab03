//Q2
const mysteryStd = document.querySelectorAll('p')[2];
mysteryStd.textContent = 'Lucille Piccio';

//Q3a
//saving aside element in a variable
const hoverMessage = document.getElementById('hover');

//no need to pass through parameters
function showAside(){
  hoverMessage.style.visibility = 'visible';

}

function hideAside(){
  hoverMessage.style.visibility = 'hidden';
}

//Q3b
const article = document.getElementById('maintext');

//Q3c
article.addEventListener("mouseover", showAside);
article.addEventListener("mouseout", hideAside);

//Q4a
mysteryStd.style.fontSize = '10pt';
mysteryStd.style.fontsize =' 14pt';
